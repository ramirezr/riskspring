package com.example.risk.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.risk.Model.Continent;

public interface ContinentRepository extends JpaRepository<Continent, Integer> {

	
	@Query("SELECT c FROM Continent c WHERE PropietariID=:CodiJugador")
	List <Continent> LlistarContinentsJugador(@Param("CodiJugador") int CodiJugador);
	
	@Query("SELECT count(*) FROM Continent c WHERE PropietariID=:codiJugador")
	int comptarContinents(@Param("codiJugador") int codiJugador);
	
}
