package com.example.risk.Repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.example.risk.Model.Jugador;

public interface JugadorRepository extends JpaRepository<Jugador, Integer>  {
	
	

}
