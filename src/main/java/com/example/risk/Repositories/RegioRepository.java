package com.example.risk.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.risk.Model.Regio;

public interface RegioRepository extends JpaRepository<Regio, Integer> {
	
	
	@Query(value = "SELECT * FROM riskspring.Regio WHERE PropietariID=:CodiJugador",nativeQuery = true)
	List <Regio> LlistarRegionsJugador(@Param("CodiJugador") int CodiJugador);
	
	@Query("SELECT count(r) FROM Regio r WHERE PropietariID=:CodiJugador")
	int ComptarRegions(@Param("CodiJugador") int CodiJugador);
	
}
