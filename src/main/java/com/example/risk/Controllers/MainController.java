package com.example.risk.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.risk.Model.Continent;
import com.example.risk.Model.Jugador;
import com.example.risk.Model.Regio;
import com.example.risk.Services.ContinentServices;
import com.example.risk.Services.JugadorServices;
import com.example.risk.Services.RegioServices;


@Controller
public class MainController {

	@Autowired
	ContinentServices cServices;
	
	@Autowired
	JugadorServices jServices;
	
	@Autowired
	RegioServices rServices;
		
	//El main controller nomes s'encarregará de carregar la base de dades!
	@GetMapping(path="/loaddb")
	public @ResponseBody String addGameContents() {
		List<Continent> continents = new ArrayList<Continent>();
		List<Regio> rrr ;
		
		Jugador adrian = new Jugador("Adrian", 0);
		Jugador amina = new Jugador("Amina", 1);
		Jugador raul = new Jugador("Raul", 2);

		jServices.insertar(amina);
		jServices.insertar(adrian);
		jServices.insertar(raul);
		
		//creem el continent africa i les seves regions
		Continent africa = new Continent("Africa");
		cServices.insertar(africa);
		Regio marruecos = new Regio("Marruecos", africa, adrian);
		adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
		rServices.insertar(marruecos);
		Regio congo = new Regio("Congo", africa, raul);
		raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
		rServices.insertar(congo);
		Regio algeria = new Regio("Algeria", africa, amina);
		amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
		rServices.insertar(algeria);
		Regio mali = new Regio("Mali", africa, amina);
		amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
		rServices.insertar(mali);
		Regio libia = new Regio("Libia", africa, adrian);
		adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
		rServices.insertar(libia);
		rrr = rServices.findAll();
		for(Regio r : rrr) {
			if(r.getContinent().equals(africa)) {
				africa.getRegions().add(r);
			}
		}
		for (Regio r : africa.getRegions()) {
			r = setVeins(africa, r);
		}
		continents.add(africa);

		//creem el continent africa
		Continent europa = new Continent("Europa");
		cServices.insertar(europa);
		Regio espanya = new Regio("Espanya", europa, adrian);
		adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
		rServices.insertar(espanya);
		Regio francia = new Regio("Francia", europa, amina);
		amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
		rServices.insertar(francia);
		Regio alemania = new Regio("Alemania", europa, raul);
		raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
		rServices.insertar(alemania);
		Regio italia = new Regio("Italia", europa, raul);
		raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
		rServices.insertar(italia);
		Regio austria = new Regio("Austria", europa, amina);
		amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
		rServices.insertar(austria);
		rrr = rServices.findAll();
		for(Regio r : rrr) {
			if(r.getContinent().equals(europa)) {
				europa.getRegions().add(r);
			}
		}
		for (Regio r : europa.getRegions()) {
			r = setVeins(europa, r);
		}
		continents.add(europa);
		
		//creem el continent asia
		Continent asia = new Continent("Asia");
		cServices.insertar(asia);
		Regio afganistan = new Regio("Afganistan", asia, adrian);
		adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
		rServices.insertar(afganistan);
		Regio bangladesh = new Regio("Bangladesh", asia, raul);
		raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
		rServices.insertar(bangladesh);
		Regio butan = new Regio("Butan", asia, adrian);
		adrian.setQuantitatRegions(adrian.getQuantitatRegions() + 1);
		rServices.insertar(butan);
		Regio camboya = new Regio("Camboya", asia, amina);
		amina.setQuantitatRegions(amina.getQuantitatRegions() + 1);
		rServices.insertar(camboya);
		Regio china = new Regio("China", asia, raul);
		raul.setQuantitatRegions(raul.getQuantitatRegions() + 1);
		rServices.insertar(china);
		
		rrr = rServices.findAll();
		for(Regio r : rrr) {
			if(r.getContinent().equals(asia)) {
				asia.getRegions().add(r);
			}
		}
		
		for (Regio r : asia.getRegions()) {
			r = setVeins(asia, r);
		}
		continents.add(asia);
		
		//asia relacio amb europa i africa
		china.getVeins().add(marruecos);
		afganistan.getVeins().add(alemania);
		espanya.getVeins().add(marruecos);
		marruecos.getVeins().add(alemania);
		austria.getVeins().add(congo);
		
		rServices.editar(china);
		rServices.editar(afganistan);
		rServices.editar(austria);
		rServices.editar(alemania);
		rServices.editar(congo);
		rServices.editar(espanya);
		rServices.editar(marruecos);		
		jServices.editar(amina);
		jServices.editar(adrian);
		jServices.editar(raul);
		
		return "Todo ha sido creado perfectamente!";
	}

	private static Regio setVeins(Continent c, Regio r) {
		for (Regio rr : c.getRegions()) {
			if (!rr.equals(r)) {
				if(!rr.getVeins().contains(r)) {
					r.getVeins().add(rr);
				}
			}
		}
		return r;
	}
}
