package com.example.risk.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.risk.Model.Regio;
import com.example.risk.Services.RegioServices;

@Controller
public class RegioController {

	@Autowired
	RegioServices rServices;

	@GetMapping(path = "/RegionsJugador/{codiJugador}")
	public @ResponseBody List<Regio> getRegionsJugador(@PathVariable int codiJugador) {
		return rServices.LlistarRegionsJugador(codiJugador);
	}

	@GetMapping(path = "/ComptarRegions/{codiJugador}")
	public @ResponseBody String getCountRegionsJugador(@PathVariable int codiJugador) {
		return "El jugador te " + rServices.ComptarRegions(codiJugador) + " regions.";
	}

    @GetMapping(path="/PosarTropes")
    public @ResponseBody String posarTropes(@RequestParam int CodiUsuari, @RequestParam int Tropes){   
        return rServices.posarTropes(CodiUsuari, Tropes);
    }
	
	@GetMapping(path = "/Reestructurar")
	public @ResponseBody String faseReestructurar(@RequestParam int CodiRegioMoure, @RequestParam int CodiRegioRebre) {
		return rServices.faseReestructuracio(CodiRegioMoure, CodiRegioRebre);
	}

	@GetMapping(path = "/RegionsAtacables/{codiJugador}")
	public @ResponseBody Set<Regio> getRegionsAtacar(@PathVariable int codiJugador) {
		return rServices.MostrarRegionsAtacar(codiJugador);
	}

	@GetMapping(path = "/Atac")
	public @ResponseBody String atacar(@RequestParam int CodiRegioAtacant, @RequestParam int CodiRegioAtacada) {
		return rServices.atac(CodiRegioAtacant, CodiRegioAtacada);
	}

	@GetMapping(path = "/TirarDaus")
	public @ResponseBody List<String> tirarDaus(@RequestParam int TropesAtacant, @RequestParam int TropesAtacada) {
		List<Integer> dausResultat = rServices.tirarDaus(TropesAtacant, TropesAtacada);
		List<String> result = new ArrayList<>();
		result.add("Atacant ataca amb " + dausResultat.get(0) + " tropes.");
		result.add("Defensor defensa amb " + dausResultat.get(1) + " tropes.");
		result.add("Resultat despres de l'atac: ");
		result.add("Atacant queda amb " + dausResultat.get(2) + " tropes.");
		result.add("Defensor queda amb " + dausResultat.get(3) + " tropes.");
		return result;
	}

}
