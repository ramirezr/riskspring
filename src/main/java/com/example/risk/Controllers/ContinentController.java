package com.example.risk.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.risk.Model.Continent;
import com.example.risk.Services.ContinentServices;

@Controller
public class ContinentController {
	
	@Autowired
	ContinentServices cServices;
	
	@GetMapping(path="/ContinentsJugador/{codiJugador}")
	public @ResponseBody List<Continent> getContinentsJugador(@PathVariable int codiJugador) {
		return cServices.LlistarContinentsJugador(codiJugador);
	}
	
	@GetMapping(path="/ComptarContinents/{codiJugador}")
	public @ResponseBody String getCountContinentJugador(@PathVariable int codiJugador){
		return "El jugador te " + cServices.comptarContinent(codiJugador) + " continents.";
	}
	
	@GetMapping(path="/CanviarPropietariContinent")
	public @ResponseBody String canviarPropietariContinent(){
		return String.join("\n", cServices.canviarPropietariContinent());
	}
}
