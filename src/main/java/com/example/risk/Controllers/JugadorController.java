package com.example.risk.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.risk.Services.JugadorServices;

@Controller
public class JugadorController {

	@Autowired
	JugadorServices jServices;
	
	@GetMapping(path="/CalcularTropes/{codiJugador}")
	public @ResponseBody String getCalcTropes(@PathVariable int codiJugador){
				
		return "El jugador rep " +jServices.calcularTropes(codiJugador) + " tropes.";
	}
	
	@GetMapping(path="/FinalitzarPartida")
	public @ResponseBody String finalitzarPartida(){		
		return jServices.FinalitzarPartida()
				? "Partida finalizada" 
						: "La partida aun no ha acabado";
	}

}