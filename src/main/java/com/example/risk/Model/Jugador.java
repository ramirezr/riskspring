package com.example.risk.Model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import net.bytebuddy.asm.Advice.Local;
@Entity
@Table(name="Jugador")
@Transactional
public class Jugador implements Serializable, Comparable{
	@Id
	@Column(name="IdJugador")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="Nom",nullable=false,length=25)
	private String nom;
	@Column(name="OrdreTirada",nullable=false)
	private int ordreTirada;
	
	/**
	 * la quantitat de regions pot ser donada pel size de
	 * Set<Regio>.
	 */
	@Column(name="QuantitatRegions",nullable=false)
	private int quantitatRegions;
	@Column(name="ComptadorVictories",nullable=false)
	private int comptadorVictories;
	
	@OneToMany(mappedBy="propietariRegio", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Regio> regions;
	/**
	 * check si tenemos que cambiar el nombre de mappedBy ya que se repite con el de arriba
	 */
	@OneToMany(mappedBy="propietariContinent",cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	
	private Set<Continent> continents;
	
	
	public Jugador() {
		super();
		regions = new HashSet<Regio>();
		continents = new HashSet<Continent>();
	}

	public Jugador(String nom, int ordreTirada) {
		super();
		this.nom = nom;
		this.ordreTirada = ordreTirada;
		this.quantitatRegions = 0;
		this.comptadorVictories = 0;
		regions = new HashSet<Regio>();
		continents = new HashSet<Continent>();
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getOrdreTirada() {
		return ordreTirada;
	}

	public void setOrdreTirada(int ordreTirada) {
		this.ordreTirada = ordreTirada;
	}

	public int getQuantitatRegions() {
		return quantitatRegions;
	}

	public void setQuantitatRegions(int quantitatRegions) {
		this.quantitatRegions = quantitatRegions;
	}

	public int getComptadorVictories() {
		return comptadorVictories;
	}

	public void setComptadorVictories(int comptadorVictories) {
		this.comptadorVictories = comptadorVictories;
	}

	public Set<Regio> getRegions() {
		return regions;
	}

	public void setRegions(Set<Regio> regions) {
		this.regions = regions;
	}

	public Set<Continent> getContinents() {
		return continents;
	}

	public void setContinents(Set<Continent> continents) {
		this.continents = continents;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jugador other = (Jugador) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

	@Override
	public String toString() {
		return "Jugador [id=" + id + ", nom=" + nom + ", ordreTirada=" + ordreTirada + ", quantitatRegions="
				+ quantitatRegions + ", comptadorVictories=" + comptadorVictories + "]";
	}

	public int compareTo(Object o) {
		Jugador j = (Jugador)o;
		int ordreLocal = this.ordreTirada;
		int ordreExt = (int)j.getOrdreTirada();
		return (ordreLocal < ordreExt) ? -1 : ((ordreLocal == ordreExt) ? 0 : 1);
	}


	
}
