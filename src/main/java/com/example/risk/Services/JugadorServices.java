package com.example.risk.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.risk.Model.Continent;
import com.example.risk.Model.Jugador;
import com.example.risk.Model.Regio;
import com.example.risk.Repositories.ContinentRepository;
import com.example.risk.Repositories.JugadorRepository;
import com.example.risk.Repositories.RegioRepository;

@Service
public class JugadorServices {

	@Autowired
	JugadorRepository jRepository;
	@Autowired
	ContinentRepository cRepository;

	@Autowired
	RegioRepository rRepository;

	public Jugador findById(Integer id) {
		return jRepository.findById(id).orElse(null);
	}

	public List<Jugador> findAll() {
		return jRepository.findAll();
	}

	/*
	 * public void delete(Integer id) {
	 * 
	 * jRepository.deleteById(id); }
	 */

	public Jugador insertar(Jugador j) {
		return jRepository.save(j);
	}

	public Jugador editar(Jugador j) {
		return jRepository.save(j);
	}

	
	
	//Falta metodo que recupera jugador pel nom para añadirlo en el print
	public int calcularTropes(Integer codiJugador) {
		//utilitzem funcions  del programa per fer el calcul
		int continents = cRepository.comptarContinents(codiJugador);
		int regions = rRepository.ComptarRegions(codiJugador);
		
		
		int quantitatTropes = 5 + (regions/3) + continents;	
		return quantitatTropes;
	}
	
	public boolean FinalitzarPartida() {
		List<Jugador> jgs = jRepository.findAll();
		int totalContinents = cRepository.findAll().size();
		boolean state = false;
		
		for(Jugador j : jgs) {
			if(cRepository.LlistarContinentsJugador(j.getId()).size() == totalContinents) {
				state = true;
				break;
			}
		}
		return state;
	}
}
