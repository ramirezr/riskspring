package com.example.risk.Services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.risk.Model.Continent;
import com.example.risk.Model.Jugador;
import com.example.risk.Model.Regio;
import com.example.risk.Repositories.ContinentRepository;


@Service
public class ContinentServices {
	@Autowired
	ContinentRepository cRepository;
	
	public Continent findById(Integer id) {
		return cRepository.findById(id).orElse(null);
	}

	public List<Continent> findAll() {
		return cRepository.findAll();
	}

	/*public void delete(Integer id) {

		jRepository.deleteById(id);
	}*/

	public Continent insertar(Continent c) {
		return cRepository.save(c);
	}

	public Continent editar(Continent c) {
		return cRepository.save(c);
	}
	
	//Lista continentes con propietario :CodiJugador
	public List<Continent> LlistarContinentsJugador(int codiJugador){
		return cRepository.LlistarContinentsJugador(codiJugador);
	}
	
	//printa continents en el controller
	public int comptarContinent(Integer codiJugador) {
		return cRepository.comptarContinents(codiJugador);
	}
	
	//Comprueba si ha habido algun cambio en los propietarios de los Continentes
	public Set<String> canviarPropietariContinent() {
		Set<String> msgs = new HashSet<>();
		List<Continent> continents;
		Jugador j = null;
		int cont = 0;
		continents = cRepository.findAll();
		for (Continent c : continents) {
			cont = 0;
			if (c.getRegions() != null) {
				for (Regio r : c.getRegions()) {
					if (r.getPropietariRegio() != null && j == null) {
						j = r.getPropietariRegio();
					}
					if ((j != null && r.getPropietariRegio() != null) && j.equals(r.getPropietariRegio())) {
						cont++;
					}
				}
				if (cont == c.getRegions().size()) {
					if (c.getPropietari() != null && c.getPropietari().equals(j)) {
						msgs.add(c.getPropietari().getNom() + " es el propietari de " + c.getNom());
					} else {
						c.setPropietari(j);
						cRepository.save(c);
						msgs.add(c.getPropietari().getNom() + " ha ganado un nuevo continente: " + c.getNom());
					}
				} else {
					if (c.getPropietari() != null) {
						msgs.add(c.getPropietari().getNom() + " ha perdut " + c.getNom());
						c.setPropietari(null);
						cRepository.save(c);
					} else {
						msgs.add(c.getNom() + " no tiene propietario");
					}
				}
				j = null;
			}
		}
		return msgs;
	}
	
}
