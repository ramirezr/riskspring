package com.example.risk.Services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.risk.Model.Jugador;
import com.example.risk.Model.Regio;
import com.example.risk.Repositories.JugadorRepository;
import com.example.risk.Repositories.RegioRepository;

@Service
public class RegioServices {

	@Autowired
	RegioRepository rRepository;
	@Autowired
	JugadorRepository jRepository;

	private Random random = new Random();

	public Regio findById(Integer id) {
		return rRepository.findById(id).orElse(null);
	}

	public List<Regio> findAll() {
		return rRepository.findAll();
	}
	/*
	 * public void delete(Integer id) {
	 * 
	 * rRepository.deleteById(id); }
	 */

	public Regio insertar(Regio r) {
		return rRepository.save(r);
	}

	public Regio editar(Regio r) {
		return rRepository.save(r);
	}

	///////////////////////////////////////////////////////////
	public List<Regio> LlistarRegionsJugador(int CodiJugador) {
		return rRepository.LlistarRegionsJugador(CodiJugador);
	}

	public int ComptarRegions(int CodiJugador) {
		return rRepository.ComptarRegions(CodiJugador);
	}

	public String posarTropes(int CodiUsuari, int Tropes) {

		List<Regio> regionsJugador = rRepository.LlistarRegionsJugador(CodiUsuari);
		String result = "";
		if (!regionsJugador.isEmpty()) {
			System.out.println();
			while (Tropes > 0) {
				int tropesAEntregar = random.nextInt(Tropes) + 1;
				int regioRandom = random.nextInt(regionsJugador.size());
				Regio r = regionsJugador.get(regioRandom);
				int tropesActuals = r.getTropes() + tropesAEntregar;
				r.setTropes(tropesActuals);
				rRepository.save(r);
				Tropes -= tropesAEntregar;
				result = result + "S'ha afegit " + tropesAEntregar + " tropes a " + r.getNom() + ".</br>";

			}
			return result;
		}

		return "El jugador no té regions!";
	}

	public String faseReestructuracio(int CodiRegioMoure, int CodiRegioRebre) {
		Regio regioMoure = rRepository.getById(CodiRegioMoure);
		Regio regioRebre = rRepository.getById(CodiRegioRebre);
		Set<Regio> veinesMoure = regioMoure.getVeins();
		Set<Regio> veinesRebre = regioMoure.getVeins();

		if (veinesMoure.contains(regioRebre) || veinesRebre.contains(regioMoure)) {
			int tropesRegioMoure = regioMoure.getTropes();

			if (tropesRegioMoure > 1) {
				if (regioMoure.getPropietariRegio().equals(regioRebre.getPropietariRegio())) {
					int tropesAPassar = random.nextInt(tropesRegioMoure) + 1;

					while (tropesAPassar == tropesRegioMoure) {
						tropesAPassar = random.nextInt(tropesRegioMoure) + 1;
					}

					regioMoure.setTropes(regioMoure.getTropes() - tropesAPassar);
					regioRebre.setTropes(regioRebre.getTropes() + tropesAPassar);

					rRepository.save(regioRebre);
					rRepository.save(regioMoure);

					System.out
							.println("El jugador " + regioMoure.getPropietariRegio().getNom() + " vol moure tropes...");
					return (regioMoure.getPropietariRegio().getNom() + " ha mogut " + tropesAPassar + " tropes de "
							+ regioMoure.getNom() + " a " + regioRebre.getNom());
				}
				return " Les regions han de pertànyer al mateix jugador.";
			}
			return "El jugador no té tropes suficients per moure!";
		} 
		return "Les regions no son veines";
	}

	public Set<Regio> MostrarRegionsAtacar(int CodiUsuari) {

		List<Regio> regionsJugador = LlistarRegionsJugador(CodiUsuari);
		Set<Regio> rAtacables = new HashSet<Regio>();

		for (Regio r : regionsJugador) {
			for (Regio rv : r.getVeins()) {
				if (rv.getPropietariRegio().getId() != CodiUsuari) {
					rAtacables.add(rv);
				}
			}
		}
		return rAtacables;
	}

	public String atac(int CodiRegioAtacant, int CodiRegioAtacada) {
		String result = null;

		Regio rAtacant = findById(CodiRegioAtacant);
		Regio rDefensor = findById(CodiRegioAtacada);

		if (rAtacant == null && rDefensor == null) { // si ni la region atacante ni la defensora existen
			result = "Regio Atacant i Defensora no existeixen";
		} else if (rAtacant == null) { // si no existe la region atacante
			result = "Regio Atacant no existeix";
		} else if (rDefensor == null) { // si no existe la region defensora
			result = "Regio Defensora no existeix";
		} else if (rAtacant.equals(rDefensor)) {// si les dues regions son la mateixa
			result = "No pots atacar-te a tu mateix!";
		} else if (rAtacant.getTropes() <= 1) { // si la region atacante tiene 1 tropa no puede atacar
			result = "Regio Atacant no te tropes suficients";
		} else {
			Set<Regio> rVeines = rAtacant.getVeins();
			Set<Regio> rVeinesDefensor = rDefensor.getVeins();
			if (rVeines.contains(rDefensor) || rVeinesDefensor.contains(rAtacant)) {
				if (rDefensor.getPropietariRegio().getId() == rAtacant.getPropietariRegio().getId()) {
					result = "Les Regions son del mateix propietari...";
				}

				else {
					int tropesAtacant;
					int tropesDefensor;

					int tropesAtacantAux;
					int tropesDefendentAux;

					Jugador jAtacant = rAtacant.getPropietariRegio();
					Jugador jDefensor = rDefensor.getPropietariRegio();

					if (rDefensor.getTropes() > 2) {
						tropesDefensor = 2;
					} else {
						tropesDefensor = rDefensor.getTropes();
					}
					if (rAtacant.getTropes() > 1 && rAtacant.getTropes() < 4) {
						tropesAtacant = rAtacant.getTropes() - 1;
					} else { // RegioAtacant.getTropes()==1
						tropesAtacant = 3;
					}

					List<Integer> resultAtac = tirarDaus(tropesAtacant, tropesDefensor);
					tropesAtacantAux = resultAtac.get(2);
					tropesDefendentAux = resultAtac.get(3);
					// METER MENSAJES//METER MENSAJES//METER MENSAJES//METER MENSAJES//METER
					// MENSAJES
					if (tropesDefendentAux > 0) {// METER MENSAJES
						rAtacant.setTropes(rAtacant.getTropes() - (tropesAtacant - tropesAtacantAux));
						rDefensor.setTropes(rDefensor.getTropes() - (tropesDefensor - tropesDefendentAux));
						result = "La Regio " + rDefensor.getNom() + " amb propietari "
								+ rDefensor.getPropietariRegio().getNom() + " defend l'atac de " + rAtacant.getNom()
								+ " amb propietari " + rAtacant.getPropietariRegio().getNom();
					} else if (tropesDefendentAux == 0) {
						result = "La Regio " + rAtacant.getNom() + " amb propietari "
								+ rAtacant.getPropietariRegio().getNom() + " guanya l'atac contra " + rDefensor.getNom()
								+ " amb propietari " + rDefensor.getPropietariRegio().getNom();
						rDefensor.setTropes(0);
						rDefensor.setPropietariRegio(rAtacant.getPropietariRegio());

						rAtacant.setTropes(rAtacant.getTropes() - (tropesAtacant - tropesAtacantAux));
						rDefensor.setTropes(rAtacant.getTropes() - 1);
						rAtacant.setTropes(1);

						jAtacant.setQuantitatRegions(jAtacant.getQuantitatRegions() + 1);
						jDefensor.setQuantitatRegions(jDefensor.getQuantitatRegions() - 1);
					}

					jRepository.save(jAtacant);
					jRepository.save(jDefensor);
					editar(rAtacant);
					editar(rDefensor);
				}
			} else {
				result = "Les Regions no son veines...";
			}
		}
		return result;
	}

	public List<Integer> tirarDaus(int TropesAtacant, int TropesAtacada) {
		List<Integer> atacant = new ArrayList<Integer>();
		List<Integer> defendent = new ArrayList<Integer>();
		List<Integer> result = new ArrayList<Integer>();
		int diceCompare;

		if (TropesAtacant > 3) { // Asegurem que nomes es pot atacar amb un maxim de 3 tropes
			TropesAtacant = 3;
		}
		if (TropesAtacada > 2) { // Asegurem que nomes es pot defençar amb un maxim de 2 tropes
			TropesAtacada = 2;
		}
		// guardem amb cuantes tropes começem atacant i defendent
		// per despres printar al controller.

		int tropesAtacantAux = TropesAtacant;
		int tropesDefendentAux = TropesAtacada;

		for (int i = 0; i < TropesAtacant; i++) { // afegim daus depenent del nombre de tropes que l'atacant té.
			atacant.add(random.nextInt(6) + 1);
		}
		Collections.sort(atacant, Collections.reverseOrder()); // les ordenem de mes gran a mes petit per comparar mes
		System.out.println("Resultat dels daus de latacant: " + atacant); // tard.

		for (int i = 0; i < TropesAtacada; i++) { // afegim daus depenent del nombre de tropes que el defensor té.
			defendent.add(random.nextInt(6) + 1);
		}
		Collections.sort(defendent, Collections.reverseOrder()); // les ordenem de mes gran a mes petit per comparar mes
		System.out.println("Resultat dels daus del defensor: " + defendent); // tard.

		// cuants daus hem de comparar?
		diceCompare = Math.min(atacant.size(), defendent.size());

		// si dau atacant es mes gran que el dau del defensor, l'atacant mata una tropa,
		// si el del defensor es igual o major al de l'atacant, mata una tropa
		for (int i = 0; i < diceCompare; i++) {
			if (atacant.get(i) > defendent.get(i)) {
				TropesAtacada--;
			} else {
				TropesAtacant--;
			}
		}
		result.add(tropesAtacantAux);
		result.add(tropesDefendentAux);
		result.add(TropesAtacant);
		result.add(TropesAtacada);

		return result;
	}
}