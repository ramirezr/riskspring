package com.example.risk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiskSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiskSpringApplication.class, args);
	}

}
