//1. Primer sempre hem de carregar la base de dades amb els jugador i regions i continents.
http://localhost:9001/loaddb


//2. Podem utilitzar qualsevol dels metodes a continuació:

//JUGADOR
l. finalitzarPartida()
http://localhost:9001/FinalitzarPartida

e. calcularTropes(int codiJugador)
http://localhost:9001/CalcularTropes/1



//REGIONS
b. llistarRegionsJugador(int codiJugador)
http://localhost:9001/RegionsJugador/1

c. comptarRegions(int codiJugador)
http://localhost:9001/ComptarRegions/1

f. posarTropes(int codiUsuari, int tropes)
http://localhost:9001/PosarTropes?CodiUsuari=3&Tropes=7

k. faseReestructuracio(int codiRegioMoure, int codiRegioRebre)
	//En aquest exemple les regions son del mateix jugador:
http://localhost:9001/Reestructurar?CodiRegioMoure=1&CodiRegioRebre=5

	//En aquest exemple no son del mateix jugador
http://localhost:9001/Reestructurar?CodiRegioMoure=8&CodiRegioRebre=7

g. mostartRegionsAtacables(int codiJugador)
http://localhost:9001/RegionsAtacables/1

i. tirarDaus(int TropesAtacant, int TropesAtacada)
http://localhost:9001/TirarDaus?TropesAtacant=5&TropesAtacada=2

h. atac(int codiRegioAtacant, int codiRegioAtacada)
	//En aquest exemple les regions no son veines
http://localhost:9001/Atac?CodiRegioAtacant=11&CodiRegioAtacada=3

	//En aquest exemple son veines
http://localhost:9001/Atac?CodiRegioAtacant=1&CodiRegioAtacada=2



//CONTINENTS
d. comptarContinents(int codiJugador)
http://localhost:9001/ComptarContinents/1

a. llistarContinentsJugador(int codiJugador)
http://localhost:9001/ContinentsJugador/1

j. canviarPropietariContinent()
http://localhost:9001/CanviarPropietariContinent


